require('dotenv').config()
const express = require('express')

const { PORT } = process.env
const app = express()

app.use(require('./route/rootRoute'))

const port = PORT || 3000
app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
})